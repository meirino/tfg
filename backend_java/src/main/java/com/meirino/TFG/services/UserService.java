package com.meirino.TFG.services;

import com.meirino.TFG.entities.*;
import com.meirino.TFG.repositories.ExerciseRepository;
import com.meirino.TFG.repositories.LessonRepository;
import com.meirino.TFG.repositories.TrophyRepository;
import com.meirino.TFG.repositories.UserRepository;
import com.meirino.TFG.utils.LoginResponse;
import com.meirino.TFG.utils.StringRedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UserService {

//    private final PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private StringRedisRepository redisRepository;
    private ExerciseRepository exerciseRepository;
    private LessonRepository lessonRepository;
    private TrophyRepository trophyRepository;

    @Autowired
    public UserService(
//            PasswordEncoder passwordEncoder,
            StringRedisRepository redisRepository,
            UserRepository userRepository,
            ExerciseRepository exerciseRepository,
            LessonRepository lessonRepository,
            TrophyRepository trophyRepository
    ) {
//        this.passwordEncoder = passwordEncoder;
        this.redisRepository = redisRepository;
        this.userRepository = userRepository;
        this.exerciseRepository = exerciseRepository;
        this.lessonRepository = lessonRepository;
        this.trophyRepository = trophyRepository;
    }
    @PostConstruct
    public void init(){
        Exercise ex1 = new Exercise("Ejercicio 1: Contraseña segura", LocalDateTime.now(), LocalDateTime.now(), "password");
        Trophies tr1 = new Trophies("Trofeo 1: Contraseña Segura", LocalDateTime.now(), LocalDateTime.now(), "silver", "ejercicio1");

        Exercise ex2 = new Exercise("Ejercicio 2: Phishing", LocalDateTime.now(), LocalDateTime.now(), "phishing");
        Trophies tr2 = new Trophies("Trofeo 2: Phishing", LocalDateTime.now(), LocalDateTime.now(), "silver", "ejercicio2");

        this.userRepository.save(new User("jjmeirino@gmail.com", "6121994spore", "Meirino", ""));
        this.exerciseRepository.save(ex1);
        this.trophyRepository.save(tr1);
        this.exerciseRepository.save(ex2);
        this.trophyRepository.save(tr2);

//        User u = this.userRepository.findByEmail("jjmeirino@gmail.com");
//        u.addExercise(ex1);
//        this.userRepository.save(u);
    }


    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    public User register(User user) {

        // Modificaciones básicas
        user.setMfa_activated(false);
//        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // Guardar el usuario en base de datos
        this.userRepository.save(user);
        return user;
    }

    public LoginResponse login(String uuid, String email, String pass) throws NullPointerException, IllegalAccessException {
        try {
            // Encontrar usuario por email
            User user = this.userRepository.findByEmail(email);
            // Comparar contraseñas
            if(user.getPassword().equals(pass)) {
                // Añadir a Redis
                this.redisRepository.add(uuid, user.getId().toString());
                return new LoginResponse(uuid, user);
            } else {
                throw new IllegalAccessException();
            }
        } catch (Exception e) {
            throw  e;
        }
    }

    public boolean tokenExists(String uuid) throws NullPointerException, IllegalAccessException {
        try {
            return this.redisRepository.getBy(uuid) != null;
        } catch (NullPointerException npe) {
            throw npe;
        }
    }

    public LoginResponse returnUser(String uuid) throws NullPointerException, IllegalAccessException {
        try {
            String userId = this.redisRepository.getBy(uuid);
            Long id = Long.parseLong(userId);
            User u = this.userRepository.getOne(id);
            return new LoginResponse(uuid, u);
        } catch (NullPointerException npe) {
            throw npe;
        }
    }

    public boolean editUser(int id, String email, String username) throws NullPointerException {
        try {
            Optional<User> user = this.userRepository.findById((long) id);
            if (user.isPresent()) {
                User usuario = user.get();
                if(!email.isEmpty()) usuario.setEmail(email);
                if(!username.isEmpty()) usuario.setUsername(username);
                this.userRepository.save(usuario);
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public boolean editPass(int id, String password) {
        try {
            Optional<User> user = this.userRepository.findById((long) id);
            if (user.isPresent()) {
                User usuario = user.get();
                usuario.setPassword(password);
                this.userRepository.save(usuario);
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public Boolean signout(String uuid) throws NullPointerException {
        try {
            redisRepository.delete(uuid);
            return true;
        } catch (NullPointerException npe) {
            throw npe;
        }
    }

    public Boolean addExercise(String exid, int uid) {
        try {
            Exercise ex = exerciseRepository.findByExternalId(exid);
            Optional<User> user = userRepository.findById((long) uid);
            if (user.isPresent()) {
                User usuario = user.get();
                if(!usuario.getExercises().contains(ex)) {
                    usuario.addExercise(ex);
                }
                this.userRepository.save(usuario);
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public Boolean addLesson(int leid, int uid) {
        try {
            Lesson le = lessonRepository.getOne((long) leid);
            Optional<User> user = userRepository.findById((long) uid);
            if (user.isPresent()) {
                User usuario = user.get();
                if(!usuario.getLessons().contains(le)) {
                    usuario.addLesson(le);
                }
                this.userRepository.save(usuario);
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public Boolean addTrophy(String trid, int uid) {
        try {
            Trophies tr = trophyRepository.findByExternalId(trid);
            Optional<User> user = userRepository.findById((long) uid);
            if (user.isPresent()) {
                User usuario = user.get();
                if(!usuario.getTrophies().contains(tr)) {
                    usuario.addTrophie(tr);
                }
                this.userRepository.save(usuario);
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
    }

    public List<Exercise> getExercises(int uid) {
        try {
            Optional<User> user = userRepository.findById((long) uid);
            if (user.isPresent()) {
                User usuario = user.get();
                return usuario.getExercises();
            } else {
                return new ArrayList<>();
            }
        } catch (NullPointerException npe) {
            return new ArrayList<>();
        }
    }

    public List<Integer> getProgress(int uid) {
        try {
            Optional<User> user = userRepository.findById((long) uid);
            if (user.isPresent()) {
                User usuario = user.get();
                return new ArrayList<>(Arrays.asList(usuario.getLessons().size(), usuario.getExercises().size(), usuario.getTrophies().size()));
            } else {
                return new ArrayList<>();
            }
        } catch (NullPointerException npe) {
            return new ArrayList<>();
        }
    }

    public Map<String, List<? extends AbstractProgress>> getCompleteProgress(int uid) {
        try {
            Optional<User> user = userRepository.findById((long) uid);
            if (user.isPresent()) {
                User usuario = user.get();
                List<Exercise> completedEx = user.get().getExercises();
                List<Lesson> completedLes = user.get().getLessons();
                List<Trophies> completedTro = user.get().getTrophies();

                Map<String, List<? extends AbstractProgress>> total = new HashMap<>();
                total.put("exe", completedEx);
                total.put("les", completedLes);
                total.put("tro", completedTro);

                return total;
            } else {
                return new HashMap<>();
            }
        } catch (NullPointerException npe) {
            return new HashMap<>();
        }
    }

    public List<Exercise> getAllExercises() {
        return this.exerciseRepository.findAll();
    }

    public void save(User usuario) {
        this.userRepository.save(usuario);
    }

    public Optional<User> findById(long uid) {
        return this.userRepository.findById(uid);
    }

    public void changeAvatar(int uid, String ruta) {
        User u = this.userRepository.getOne((long) uid);
        u.setAvatar_url(ruta);
        this.userRepository.save(u);
    }
}
