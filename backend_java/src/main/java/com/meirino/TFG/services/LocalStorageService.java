package com.meirino.TFG.services;

import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service
public class LocalStorageService implements StorageService {

    private final Path root = Paths.get("C:\\Projects\\chatbot-tfg\\public\\media\\avatars");
//    private final Path root = Paths.get("/home/ec2-user/nodeapp/dist/media/avatars");

    public LocalStorageService() {}

    @Override
    public void init() {
        try {
            Files.createDirectory(root);
        } catch (IOException e) {
            throw new RuntimeException("No se ha podido inicializar la carpeta de subida.");
        }
    }

    @Override
    public String save(MultipartFile file) {
        try {
            Files.copy(file.getInputStream(), this.root.resolve(Objects.requireNonNull(file.getOriginalFilename())));
            return file.getOriginalFilename();
        } catch (Exception e) {
            throw new RuntimeException("No se ha podido guardar el archivo. Error: " + e.getMessage());
        }
    }

    @Override
    public void clean() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }


}
