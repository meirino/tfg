package com.meirino.TFG.services;

import com.meirino.TFG.entities.Lesson;
import com.meirino.TFG.entities.Trophies;
import com.meirino.TFG.entities.User;
import com.meirino.TFG.repositories.ExerciseRepository;
import com.meirino.TFG.repositories.LessonRepository;
import com.meirino.TFG.repositories.TrophyRepository;
import com.meirino.TFG.utils.ExerciseForm;
import com.meirino.TFG.utils.StringRedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class LessonService {
    //    private final PasswordEncoder passwordEncoder;
    private UserService userService;
    private StringRedisRepository redisRepository;
    private ExerciseRepository exerciseRepository;
    private LessonRepository lessonRepository;
    private TrophyRepository trophyRepository;

    @Autowired
    public LessonService (
//          PasswordEncoder passwordEncoder,
            StringRedisRepository redisRepository,
            UserService userService,
            ExerciseRepository exerciseRepository,
            LessonRepository lessonRepository,
            TrophyRepository trophyRepository
    ) {
//        this.passwordEncoder = passwordEncoder;
        this.redisRepository = redisRepository;
        this.userService = userService;
        this.exerciseRepository = exerciseRepository;
        this.lessonRepository = lessonRepository;
        this.trophyRepository = trophyRepository;
    }

    @PostConstruct
    public void init() {
        Lesson l1 = new Lesson("Contraseñas seguras", LocalDateTime.now(), LocalDateTime.now(), "sec_password");
        Lesson l2 = new Lesson("Amenazas online: Phising", LocalDateTime.now(), LocalDateTime.now(), "phising");
        Lesson l3 = new Lesson("Proteger tu equipo", LocalDateTime.now(), LocalDateTime.now(), "defense");

        this.lessonRepository.save(l1);
        this.lessonRepository.save(l2);
        this.lessonRepository.save(l3);
    }

    public List<Lesson> getAll() {
        return this.lessonRepository.findAll();
    }

    public Boolean addLesson(String leid, int uid) {
        try {
            Lesson le = lessonRepository.findByExternalId(leid);
            Optional<User> user = this.userService.findById(uid);
            if (user.isPresent()) {
                User usuario = user.get();
                if(!usuario.getLessons().contains(le)) {
                    usuario.addLesson(le);
                }
                this.userService.save(usuario);
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
    }
}
