package com.meirino.TFG.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Trophies", uniqueConstraints={
        @UniqueConstraint(columnNames = {"id"})
})
public class Trophies extends AbstractProgress {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column
    private LocalDateTime created;

    @Column
    private LocalDateTime updated;

    @Column
    private String type;

    @Column
    private String externalId;

    public Trophies() {}

    public Trophies(String name, LocalDateTime created, LocalDateTime updated, String type, String externalId) {
        this.name = name;
        this.created = created;
        this.updated = updated;
        this.type = type;
        this.externalId = externalId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}