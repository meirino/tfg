package com.meirino.TFG.entities;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Users", uniqueConstraints={
        @UniqueConstraint(columnNames = {"id", "email"})
})


public class User {

    public interface userLogin {};

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(userLogin.class)
    private Long id;

    @Column(nullable = false)
    @JsonView(userLogin.class)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column
    @JsonView(userLogin.class)
    private String username;

    @Column
    private LocalDateTime created;

    @Column
    private LocalDateTime updated;

    @Column
    @JsonView(userLogin.class)
    private String avatar_url;

    @Column(nullable = false)
    private boolean mfa_activated;

    @Column(nullable = true)
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Exercise> exercises;

    @Column(nullable = true)
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Lesson> lessons;

    @Column(nullable = true)
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Trophies> trophies;

    public User(String email, String password, String username, String avatar_url) {

        this.email = email;
        this.password = password;
        this.username = username;
        this.avatar_url = avatar_url;
        this.mfa_activated = false;
        this.created = LocalDateTime.now();
        this.updated = LocalDateTime.now();
        this.exercises = new ArrayList<>();
    }

    public User() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public boolean isMfa_activated() {
        return mfa_activated;
    }

    public void setMfa_activated(boolean mfa_activated) {
        this.mfa_activated = mfa_activated;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public List<Trophies> getTrophies() {
        return trophies;
    }

    public void setTrophies(List<Trophies> trophies) {
        this.trophies = trophies;
    }

    public void addExercise(Exercise ex) {
        this.exercises.add(ex);
    }

    public void addLesson(Lesson le) {
        this.lessons.add(le);
    }

    public void addTrophie(Trophies tr) {
        this.trophies.add(tr);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", avatar_url='" + avatar_url + '\'' +
                ", mfa_activated=" + mfa_activated +
                '}';
    }
}
