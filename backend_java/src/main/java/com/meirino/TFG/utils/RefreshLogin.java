package com.meirino.TFG.utils;

public class RefreshLogin {
    private String uuid;

    public RefreshLogin(String uuid) {
        this.uuid = uuid;
    }

    public RefreshLogin() {}

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}