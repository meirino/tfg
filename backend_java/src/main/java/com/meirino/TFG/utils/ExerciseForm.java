package com.meirino.TFG.utils;

public class ExerciseForm {
    private String token;
    private int userId;
    private String exId;

    public ExerciseForm() {}

    public ExerciseForm(String s, int u, String e) {
        this.token = s;
        this.userId = u;
        this.exId = e;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getExId() {
        return exId;
    }

    public void setExId(String exId) {
        this.exId = exId;
    }
}
