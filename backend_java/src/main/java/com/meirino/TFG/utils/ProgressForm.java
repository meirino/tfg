package com.meirino.TFG.utils;

public class ProgressForm {
    public String token;
    public int uid;

    public ProgressForm(String token, int uid) {
        this.token = token;
        this.uid = uid;
    }

    public ProgressForm() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}
