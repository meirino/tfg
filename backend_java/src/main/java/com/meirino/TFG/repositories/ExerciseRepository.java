package com.meirino.TFG.repositories;

import com.meirino.TFG.entities.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
    Exercise findByExternalId(String exid);
}
