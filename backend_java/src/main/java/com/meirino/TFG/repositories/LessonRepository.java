package com.meirino.TFG.repositories;

import com.meirino.TFG.entities.Lesson;
import com.meirino.TFG.entities.Trophies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonRepository extends JpaRepository<Lesson, Long> {
    Lesson findByExternalId(String trid);
}
