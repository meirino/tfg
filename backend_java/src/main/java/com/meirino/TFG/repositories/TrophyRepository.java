package com.meirino.TFG.repositories;

import com.meirino.TFG.entities.Trophies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrophyRepository extends JpaRepository<Trophies, Long> {
    Trophies findByExternalId(String name);
}
