package com.meirino.TFG.restControllers;

import com.meirino.TFG.entities.Exercise;
import com.meirino.TFG.entities.Lesson;
import com.meirino.TFG.services.LessonService;
import com.meirino.TFG.services.UserService;
import com.meirino.TFG.utils.ExerciseForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("/api/v1/lessons")
public class RestLessonController {

    private final LessonService lessonService;
    private UserService userService;

    @Autowired
    public RestLessonController(LessonService lessonService, UserService userService) {
        this.lessonService = lessonService;
        this.userService = userService;
    }

    @PostConstruct
    public void init() {
        //
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Lesson> getLessons() {
        return this.lessonService.getAll();
    }

    @RequestMapping(value = "/completeLesson", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> completeLesson(@RequestBody ExerciseForm form) {
        try{
            if (this.userService.tokenExists(form.getToken())) {
                boolean result = this.lessonService.addLesson(form.getExId(), form.getUserId());
                if(result) {
                    return new ResponseEntity<>(true, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
            }
        } catch (NullPointerException | IllegalAccessException e) {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
