package com.meirino.TFG.restControllers;

import com.meirino.TFG.services.LocalStorageService;
import com.meirino.TFG.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/files")
public class RestUploadController {

    private final LocalStorageService localStorageService;
    private final UserService userService;

    @Autowired
    public RestUploadController(LocalStorageService localStorageService, UserService userService) {
        this.localStorageService = localStorageService;
        this.userService = userService;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<HttpStatus> uploadAvatar(@RequestParam("file") MultipartFile file, @RequestParam("uid") int uid) {
        String ruta = this.localStorageService.save(file);
        userService.changeAvatar(uid, ruta);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
