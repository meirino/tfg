import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.config.productionTip = false;

let user = localStorage.getItem('uuid');

if (user) {
  store.dispatch('detectarUsuario', user);
  new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount('#app');
} else {
  store.dispatch('detectarUsuario', null);
  new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount('#app');
}
