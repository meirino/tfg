import Vue from 'vue'
import Vuex from 'vuex'
import {User} from "@/domain/user";
import {Exercise} from "@/domain/Exercise";
import {Trophy} from "@/domain/Trophie";
import axios from 'axios';
import router from "@/router";
import {Message} from "@/domain/Mensaje";
import {Lesson} from "@/domain/Lesson";

Vue.use(Vuex);

// const baseUrl = 'http://15.188.239.245:8081/api/v1/';
// const DialogFlowBaseURL = 'http://15.188.239.245:4000/api/v1/chat';
const baseUrl = 'http://localhost:8081/api/v1/';
const DialogFlowBaseURL = 'http://localhost:4000/api/v1/chat';

let nextContext = '';

export default new Vuex.Store({
  state: {
    user: User,
    progreso: {
      tro: [],
      exe: [],
      les: []
    },
    bot: new User('', '0', 'TFG-Bot', '', 'bot.png'),
    error: '',
    success: '',
    ongoingRequest: false,
    overlay: false,
    mensajes: [],
    lastContex: 'default'
  },
  // @ts-ignore
  mutations: {
    setRequest(state, request: boolean) {
      state.ongoingRequest = request;
    },
    setUsuario(state, user: User) {
      // @ts-ignore
      state.user = user;
    },
    setInfo(state, payload: any) {
      // @ts-ignore
      if (payload.username) state.user._name = payload.username;
      // @ts-ignore
      if (payload.file.name) state.user._avatar = payload.file.name;
    },
    setError(state, error: string) {
      state.error = error;
    },
    setSuccess(state: any, succ: string) {
      state.success = succ;
    },
    changeUser(state: any, email:string, username:string) {
      state.user.email(email);
      state.user.username(username);
    },
    setOverlay(state, overlay: boolean) {
      state.overlay = overlay;
    },
    addMessage(state, mensaje) {
      // @ts-ignore
      state.mensajes.push(mensaje);
    },
    setProgreso(state, progress: [any]) {
      // @ts-ignore
      state.progreso = progress;
    },
    setExercises(state, exercises: [any]) {
      // @ts-ignore
      state.progreso['exe'] = exercises;
    },
    setLessons(state, lessons: [any]) {
      // @ts-ignore
      state.progreso['les'] = lessons;
    },
    setTrophies(state, trophies: [any]) {
      // @ts-ignore
      state.progreso['tro'] = trophies;
    }
  },
  // @ts-ignore
  actions: {
    completeExercise(context: any, payload: any) {
      axios.put(baseUrl + 'users/completeExercise', {
        token: localStorage.getItem('uuid'),
        userId: localStorage.getItem('id'),
        exId: payload.exid
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(result => {
        if (result.data === true) {
          context.commit('setSuccess', '¡Ejercicio completado!');
        } else {
          context.commit('setError', 'Ha ocurrido un error');
        }
      })
    },
    completeTrophy(context: any, payload: any) {
      axios.put(baseUrl + 'users/completeReward', {
        token: localStorage.getItem('uuid'),
        userId: localStorage.getItem('id'),
        exId: payload.exid
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(result => {
        if (result.data === true) {
          context.commit('setSuccess', '¡Trofeo completado!');
        } else {
          context.commit('setError', 'Ha ocurrido un error');
        }
      })
    },
    getProgress(context: any, payload: any) {
      function parseExercises(data: any) {
        let ex0 = [];
        for (let ex of data) {
          ex0.push(new Exercise(ex['id'], ex['name'], new Date(), new Date()));
        }
        context.commit('setExercises', ex0);
      }
      function parseTrophies(data: any) {
        let tr0 = [];
        for (let tr of data) {
          tr0.push(new Trophy(tr['id'], tr['name'], new Date(), new Date(), tr['type']));
        }
        context.commit('setTrophies', tr0);
      }
      function parseLessons(data: any) {
        let le0 = [];
        for (let le of data) {
          le0.push(new Lesson(le['id'], le['name'], new Date(), new Date()));
        }
        context.commit('setLessons', le0);
      }

      axios.post(baseUrl + 'users/progress', {
        token: localStorage.getItem('token'),
        uid: localStorage.getItem('id')
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(result => {
        if (result.data) {
          parseExercises(result.data['exe']);
          parseTrophies(result.data['tro']);
          parseLessons(result.data['les']);
        }
      })
    },
    login: (context: any, payload: any) => {

      context.commit('setRequest', true);
      axios.post(baseUrl + 'users/login', {
        email: payload.email,
        password: payload.password
      },{
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(result => {
        context.commit('setRequest', false);

        console.log(result.data.avatar_url);

        const usuario = new User(
            result.data.token,
            result.data.user.id,
            result.data.user.username,
            result.data.user.email,
            result.data.user.avatar_url
        );

        localStorage.setItem('uuid', result.data.token);
        localStorage.setItem('id', result.data.user.id);

        context.commit('setUsuario', usuario);
        router.push('/');
      }).catch((error) => {
        context.commit('setError', 'Algo malo ha ocurrido en nuestros servidores');
        context.commit('setRequest', false);
      });
    },
    register: (context: any, payload: any) => {
      // TODO: Handle success

      if (payload.pass1 === payload.pass2 && payload.username !== '' && payload.email !== '') {
        context.commit('setRequest', true);
        axios.post(baseUrl + 'users/register', {
          'username': payload.username,
          'password': payload.pass1,
          'email': payload.email
        }, {
          headers: {
            'Content-Type': 'application/json'
          }
        }).then((result) => {
          context.commit('setSuccess', 'Usuario registrado correctamente');
          context.commit('setRequest', false);
        }).catch((error) => {
          context.commit('setError', 'Algo malo ha ocurrido en nuestros servidores');
          context.commit('setRequest', false);
        })
      } else {
        context.commit('setError', '¡Faltan datos!');
      }
    },
    logout: async () => {
      //TODO: Finish logout
      await axios.post(baseUrl + 'users/signout', {
        uuid: localStorage.getItem('uuid')
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(() => {
        localStorage.removeItem('uuid');
        localStorage.removeItem('id');
        router.push('/login');
      });
    },
    async detectarUsuario(context: any, user: string) {
      if (user != null) {
        await axios.post(baseUrl + 'users/refreshLogin', {
              uuid: user
            }, {
              headers: {
                'Content-Type': 'application/json'
              }
            }
        ).then(result => {
          context.commit('setRequest', false);

          const usuario = new User(
              result.data.token,
              result.data.user.id,
              result.data.user.username,
              result.data.user.email,
              result.data.user.avatar_url
          );

          context.commit('setUsuario', usuario);
        }).catch((error) => {
          context.commit('setError', 'Algo malo ha ocurrido en nuestros servidores');
          context.commit('setRequest', false);
        });
      } else {
        context.commit('setUsuario', null)
      }
    },
    changeData: async (context: any, payload) => {
      context.commit('setOverlay', true);
      if(payload.file) {
        console.log(payload.file.name);
        let uid = localStorage.getItem('id');
        let fd = new FormData();
        fd.append('file', payload.file);
        // @ts-ignore
        fd.append('uid', uid.toString());
        await axios.post(
            baseUrl + 'files/',
            fd,
            {headers: {'content-type': 'multipart/form-data'}}).then((response) => {
              console.log(response)
        });
      }
      await axios.put(baseUrl + 'users/editUser', {
        token: localStorage.getItem('uuid'),
        id: localStorage.getItem('id'),
        email: payload.email,
        username: payload.username
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(async () => {
        if (payload.pass1 === payload.pass2 && payload.pass1 !== '') {
          await axios.put(baseUrl + 'users/editPass', {
            token: localStorage.getItem('uuid'),
            id: localStorage.getItem('id'),
            password: payload.pass1
          }, {
            headers: {
              'Content-Type': 'application/json'
            }
          }).then(() => {});
        }
        context.commit('setOverlay', false);
        // TODO: Fix user change
        context.commit('setInfo', payload);
      });
    },
    completeLesson(context: any, payload: any) {
      axios.put(baseUrl + 'lessons/completeLesson', {
        token: localStorage.getItem('uuid'),
        userId: localStorage.getItem('id'),
        exId: payload.exid
      }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(result => {
        if (result.data === true) {
          context.commit('setSuccess', '¡Lección completada!');
        } else {
          context.commit('setError', 'Ha ocurrido un error');
        }
      })
    },
    getResponse: (context: any, query: string) => {
      axios.post(DialogFlowBaseURL, {
        mensaje: query,
        context: context.state.lastContex
      }).then(result => {
        console.log(result);
        let intent = result.data.intent;

        switch(intent) {
          case 'securePass - more - next - next': context.dispatch('completeLesson', {exid: 'sec_password'});
          case 'secureEquipment - Final': context.dispatch('completeLesson', {exid: 'defense'});
          case 'secureBrowsing - Phising Final': context.dispatch('completeLesson', {exid: 'phising'});
          default: console.log('nada');
        }

        context.commit('addMessage', new Message(result.data.text, true));
      });
    }
  },
  modules: {
  },
  getters: {
    existeUsuario(state) {
      return !(state.user === null || state.user === undefined)
    }
  }
})
