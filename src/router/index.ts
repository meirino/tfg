import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Chat from '../views/Chat.vue'
import Login from '../views/Login.vue'
import store from '@/store/index';
import Ejercicios from '../views/Ejercicios.vue';
import Ejercicio1 from "@/components/Ejercicio1.vue";
import Ejercicio2 from "@/components/Ejercicio2.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Inicio',
    meta: {
      requiresAuth: true
    },
    component: Home
  },
  {
    path: '/perfil',
    name: 'Perfil',
    meta: {
      requiresAuth: true
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Perfil.vue')
  },
  {
    path: '/chat',
    name: 'Chat',
    meta: {
      requiresAuth: true
    },
    component: Chat
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/ejercicios',
    name: 'Ejercicios',
    component: Ejercicios
  },
  {
    path: '/ejercicios/password',
    name: 'Ejercicio1',
    component: Ejercicio1
  },
  {
    path: '/ejercicios/quiz',
    name: 'Ejercicio2',
    component: Ejercicio2
  }
];

// @ts-ignore
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

// @ts-ignore
router.beforeEach((to, from, next) => {
  const rutaProtegida = to.matched.some((record: { meta: { requiresAuth: unknown } }) => record.meta.requiresAuth);
  let user = store.getters.existeUsuario;

  if(rutaProtegida && !user) {
    next({name: 'Login'});
  } else {
    next()
  }
});

export default router
