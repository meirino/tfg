export class Error {
    private _code: Number;
    private _message: String;
    private _success: Boolean;

    constructor(code: Number, message: String, success: Boolean) {
        this._code = code;
        this._message = message;
        this._success = success;
    }


    get code(): Number {
        return this._code;
    }

    set code(value: Number) {
        this._code = value;
    }

    get message(): String {
        return this._message;
    }

    set message(value: String) {
        this._message = value;
    }

    get success(): Boolean {
        return this._success;
    }

    set success(value: Boolean) {
        this._success = value;
    }
}
