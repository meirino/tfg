export class User {
    public _id: string;
    public _name: string;
    public _avatar: string;
    public _joined: Date;
    public _token: string;
    public _email: string;

    constructor(token: string, id: string, name: string, email: string, avatar: string) {
        this._id = id;
        this._name = name;
        this._email = email;
        this._avatar = avatar;
        this._joined = new Date();
        this._token = token;
    }

    get token(): string {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get joined(): Date {
        return this._joined;
    }

    set joined(value: Date) {
        this._joined = value;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set username(value: string) {
        this._name = value;
    }

    get avatar(): string {
        return this._avatar;
    }

    set avatar(value: string) {
        this._avatar = value;
    }
}
