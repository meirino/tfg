export class Lesson {
    private _id: number;
    private _name: string;
    private _created: Date;
    private _updated: Date;


    constructor(id: number, name: string, created: Date, updated: Date) {
        this._id = id;
        this._name = name;
        this._created = created;
        this._updated = updated;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get created(): Date {
        return this._created;
    }

    set created(value: Date) {
        this._created = value;
    }

    get updated(): Date {
        return this._updated;
    }

    set updated(value: Date) {
        this._updated = value;
    }
}
