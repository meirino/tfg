import {User} from "@/domain/user";


export class Message {
    public content: string;
    public bot: boolean;

    constructor(content: string, bot: boolean) {
        this.content = content;
        this.bot = bot;
    }
}
