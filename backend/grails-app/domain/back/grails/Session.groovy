package back.grails

import java.time.LocalDateTime

class Session {

    String uuid
    LocalDateTime created
    User user

    static constraints = {
        uuid unique: true, blank: false, nullable: false
        user unique: true, blank: false, nullable: true
    }

    static mapping = {
    }

    Session(user, String uuid) {
        this.user = user
        this.uuid = uuid
        this.created = LocalDateTime.now()
    }
}