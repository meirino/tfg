package back.grails

import java.time.LocalDateTime

class User {

    String username
    String email
    String password
    LocalDateTime created
    LocalDateTime updated
    String avatarUrl

    static constraints = {
        username nullable: false, blank: false, unique: false
        email unique: true, blank: false, nullable: false
        password unique: false, blank: false, nullable: false
    }

    User(String name, String email, String password) {
        this.username = name
        this.email = email
        this.password = password
        this.avatarUrl = "avatar.png"
        this.created = LocalDateTime.now()
        this.updated = LocalDateTime.now()
    }
}
