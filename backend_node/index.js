"use strict";

// Importar librerias
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

// Importar controladores
const chatController = require('./controllers/chat');

app.use("/", express.static("dist"));
app.use("/chat", express.static("dist"));
app.use("/login", express.static("dist"));
app.use("/perfil", express.static("dist"));


// Inicializar el puerto a usar para escuchar peticiones para la API
const port = 4000;

// Middleware de análisis del cuerpo de Node.js
app.use(
    cors(),
    bodyParser.urlencoded({
        extended: false
    }),
    bodyParser.json()
);

// Definir las rutas y métodos de la API
app.post("/api/v1/chat", async (req, res) => {
    console.log(req.body);
    let respuesta = await chatController.makePost(req.body.mensaje, req.body.context);
    res.status(200).send(respuesta);
});

// Escuchando nuestro servidor Node
app.listen(port, () => {
    console.log(`API REST en el puerto: http://localhost:${port}`);
});
